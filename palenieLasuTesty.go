package main

import (
	"fmt"
	"math/rand"
	"time"
)


type Tree struct {
    alive      bool
    ablaze     bool
    resistance int
}

type Coordinates struct {
    x int
    y int
}


func (t *Tree) Burning(forest [][]*Tree, coordinates Coordinates) {
    centerX, centerY := coordinates.x, coordinates.y
    minX := centerX - 1
    minY := centerY - 1
    maxX := centerX + 1
    maxY := centerY + 1
    chance_to_burn := 0.5
    time_burning := 3
    // Ensure minX, minY, maxX, maxY are within bounds of the forest
    if minX < 0 {
        minX = 0
    }
    if minY < 0 {
        minY = 0
    }
    if maxX >= len(forest) {
        maxX = len(forest) - 1
    }
    if maxY >= len(forest[0]) {
        maxY = len(forest[0]) - 1
    }

    // Burn trees in the 3x3 area centered on the tree
    for range time_burning {
        for i := minX; i <= maxX; i++ {
            for j := minY; j <= maxY; j++ {
                if forest[i][j] != nil && !forest[i][j].ablaze && forest[i][j].alive{
                    if rand.Float64() < chance_to_burn {
                        if forest[i][j].resistance <= 0 {
                            forest[i][j].ablaze = true
                            forest[i][j].Burning(forest, Coordinates{x: i, y: j})
                        } else {
                            forest[i][j].resistance--
                        }
                    }
                }
            }
        }
    }
    forest[coordinates.x][coordinates.y].alive = false
    forest[coordinates.x][coordinates.y].ablaze = false
}

func main() {
    fmt.Println("Started test")
    start := time.Now()
	for i := range(4){
		fmt.Println("Resistance ", i)
    	test(1024, i)
	}
    fmt.Println(time.Since(start))
}

func test(tries int, resistance int) {
    for j := 0; j < 10; j++ {
        average_percentage := 0.0
        for i := 0; i < tries; i++ {
            forest := burnTheForest(100, 0.1*float64(10-j), resistance)
            average_percentage += calculateLoses(forest)
        }
        fmt.Println("Zalesienie: ", 0.1*float64(10-j)*100, ",Procent przetrwania: ",average_percentage*100/float64(tries),"%")
    }
}

func burnTheForest(size int, percent_of_trees float64, resistance int) [][]*Tree {
    var forest [][]*Tree
    var percentage = percent_of_trees
    var width = size
    var height = size
    forest = make([][]*Tree, width)
    for i := range forest {
        forest[i] = make([]*Tree, height)
        for j := range forest[i] {
            if rand.Float64() < percentage {
                new_tree := Tree{alive: true, resistance: resistance}
                forest[i][j] = &new_tree
            }
        }
    }
    lightningStrike(forest)
    return forest
}

func lightningStrike(forest [][]*Tree) {
	width := rand.Intn(len(forest))
    height := rand.Intn(len(forest[0]))
    if forest[width][height] != nil {
		forest[width][height].ablaze = true
        forest[width][height].Burning(forest, Coordinates{x: width, y: height})
		} else {
			lightningStrike(forest)
		}
	}

func calculateLoses(forest [][]*Tree) float64 {
    trees := 0
    trees_alive := 0
    for i := range forest {
        for j := range forest[i] {
            if forest[i][j] != nil {
                trees++
                if forest[i][j].alive {
                    trees_alive++
                }
            }
        }
    }
    return float64(float64(trees_alive) / float64(trees))

}
