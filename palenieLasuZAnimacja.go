package main

import (
	"fmt"
	"math/rand"
	"os"
	"os/exec"
	"time"
)

	type Tree struct{
		alive bool
		ablaze bool
		resistance int
	}

	type Coordinates struct{
		x int
		y int
	}

	func(T *Tree) String() string{
		if T.ablaze{
			return "🔥"
		}
		if T.alive{
			if T.resistance == 0{
				return "🌲"
				} else{
				return "🌳"
			}
			
		}
		if !T.alive{
				return "🪵"
		}
		return ""
	}
	var amountBurning = 0
	var ready = make(chan bool, 1)

	func (t *Tree) Burning(forest [][]*Tree, coordinates Coordinates) {
		amountBurning++
		<- ready
		centerX, centerY := coordinates.x, coordinates.y
		minX := centerX - 1
		minY := centerY - 1
		maxX := centerX + 1
		maxY := centerY + 1
		chance_to_burn := 0.5
		time_burning := 3
		// Ensure minX, minY, maxX, maxY are within bounds of the forest
		if minX < 0 {
			minX = 0
		}
		if minY < 0 {
			minY = 0
		}
		if maxX >= len(forest) {
			maxX = len(forest) - 1
		}
		if maxY >= len(forest[0]) {
			maxY = len(forest[0]) - 1
		}

		// Burn trees in the 3x3 area centered on the tree
		for range(time_burning){
			for i := minX; i <= maxX; i++ {
				for j := minY; j <= maxY; j++ {
					if forest[i][j] != nil  && !forest[i][j].ablaze && forest[i][j].alive{
						if rand.Float64() < chance_to_burn{
							if forest[i][j].resistance <= 0{
								forest[i][j].ablaze = true
								time.Sleep(time.Second/2)
								go forest[i][j].Burning(forest, Coordinates{x:i,y:j})
							} else{
								forest[i][j].resistance--
							}
						}
					}
				}
			}
		}
		forest[coordinates.x][coordinates.y].alive = false
		forest[coordinates.x][coordinates.y].ablaze = false
		amountBurning--
	}
	func main(){
		burnTheForest(20,70,1)
	}
	func burnTheForest(size int, percent_of_trees int, resistance int){
		var forest [][]*Tree
		var percentage = float64(percent_of_trees)/100
		var width = size
		var heigth = size
		forest = make([][]*Tree, width)
		for i := range(width){
			forest[i] = make([]*Tree, heigth)
			for j := range(heigth){
				if rand.Float64() < percentage{
					new_tree := Tree{alive:true, resistance: resistance}
					forest[i][j] = &new_tree
				}
			}
		}
		printForest(forest)
		time.Sleep(time.Second/2)
		go lightningStrike(forest)
		printForest:
		for{
			time.Sleep(time.Second/2)
			ready <- true
			printForest(forest)
			if(amountBurning == 0){
				close(ready)
				break printForest
			}
		}
		printForest(forest)
		fmt.Println(calculateLoses(forest), "%")
	}

	func lightningStrike(forest [][]*Tree){
		width := rand.Intn(len(forest))
		height := rand.Intn(len(forest[0]))
		if forest[width][height] != nil{
			forest[width][height].ablaze = true
			printForest(forest)
			go forest[width][height].Burning(forest, Coordinates{x:width,y:height})
		} else {
			go lightningStrike(forest)
		}
	}

	func printForest(forest[][]*Tree){
		cmd := exec.Command("clear")
		cmd.Stdout = os.Stdout
        cmd.Run()
		for i := range forest {
			for j := range forest[i] {
				if(forest[i][j] == nil){
					fmt.Print("  ")
				}else{
					fmt.Print(forest[i][j])
				}		
			}
			fmt.Println()
		}
	}


	func calculateLoses(forest [][]*Tree) float64{
		trees := 0
		trees_alive := 0
		for i := range forest {
			for j := range forest[i] {
				if(forest[i][j] != nil){
					trees++
					if forest[i][j].alive{
						trees_alive++
					}
				}
			}
		}
		return float64(trees_alive)/float64(trees)*100
	}